# lbasi9 

```
program : expr

expr : binding
     | infix
     
binding : LET variable ASSIGN expr binding_body

binding_body : IN expr
             | binding
             
infix : term ((PLUS | MINUS) term)*

term : factor ((MUL | DIV) factor)*

factor : prefix
       | literal
       | LPAREN expr RPAREN
       | variable
       
prefix : PLUS factor
       | MINUS factor
       
literal : INTEGER

variable: ID

literal : INTEGER
```

# lbasi10

```
program : expr

expr : ( binding | infix ) type_annotation?

type_annotation : COLON expr
     
binding : LET variable type_annotation? ASSIGN expr binding_body
        | variable LET_ASSIGN expr binding_body

binding_body : IN expr
             | binding
             
infix : term ((PLUS | MINUS) term)*

term : factor ((MUL | DIV) factor)*

factor : prefix
       | literal
       | LPAREN expr RPAREN
       | variable
       
prefix : PLUS factor
       | MINUS factor
       
literal : INTEGER

variable: ID

literal : INTEGER
```

# Target
```
program : statement_list

statement_list : statement
               | statement statement_list

compound_statement : LPAREN statement_list RPAREN

statement : compound_statement
          | binding
          | assert
          | func_def

assignment_statement : variable ASSIGN expr

empty :

expr: term ((PLUS | MINUS) term)*

term: factor ((MUL | DIV) factor)*

factor : PLUS factor
       | MINUS factor
       | INTEGER
       | LPAREN expr RPAREN
       | variable

variable: ID
```
