# Grim

A configuration language inspired by Dhall (albeit probably turing-complete, Dhall
doesn't have the halting problem at the expense of usability).

## Inspirations

- Dhall
- [Let's build a simple interpreter log series](https://ruslanspivak.com/lsbasi-part1/) for
  the implementation.

## Run

```
cargo run -- "a := 2                                
b := 3
in a + b"
```

should display

```
Ok(Int(5))
```

Yay! (yes I'm in the very early stages of the project)
