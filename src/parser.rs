use crate::ast::{Expr, Op, Value, AST};
use crate::lexer::{Lexer, LexerError, LexerTokens, Token, TokenType};

pub struct Parser<'i> {
    tokens: LexerTokens<'i>,
    current_token: Token<'i>,
    peeked: Option<Token<'i>>,
}

impl<'i> Parser<'i> {
    pub fn new(src: &'i str) -> Result<Self, LexerError> {
        let mut tokens = Lexer::new(src).tokens();
        let current_token = tokens.next().unwrap_or_else(|| {
            Err(LexerError {
                cause: "Empty_file".to_owned(),
            })
        })?;
        Ok(Self {
            tokens,
            current_token,
            peeked: None,
        })
    }

    fn peek(&mut self) -> Result<TokenType, LexerError>{
        if self.peeked.is_some() {
            panic!("Multiple peeks in the parser");
        }
        let next = self.tokens.next().unwrap()?;
        let result = next.type_.clone();
        self.peeked = Some(next);
        Ok(result)
    } 

    fn eat(&mut self, type_: TokenType) -> Result<Token<'i>, LexerError> {
        if self.current_token.type_ == type_ {
            let next = if self.peeked.is_none() {
                self.tokens.next().unwrap()?
            } else {
                self.peeked.take().unwrap()
            };
            let token = std::mem::replace(&mut self.current_token, next);
            Ok(token)
        } else {
            Err(LexerError {
                cause: format!(
                    "bad token type {:?} at {}: {}, expecting {:?}",
                    self.current_token.type_,
                    self.current_token.range.start,
                    self.current_token.text(),
                    type_
                ),
            })
        }
    }

    /// prefix : PLUS factor
    ///        | MINUS factor
    pub fn prefix(&mut self) -> Result<Expr, LexerError> {
        if let Value::Op(op) = self.eat(TokenType::Op)?.value() {
            if (op == Op::Plus) | (op == Op::Minus) {
                Ok(Expr::UnaryOp(op, Box::new(self.factor()?)))
            } else {
                Err(LexerError {
                    cause: format!("Not a factor"),
                })
            }
        } else {
            unreachable!()
        }
    }

    /// variable: ID
    pub fn variable(&mut self) -> Result<Expr, LexerError> {
        let token = self.eat(TokenType::Ident)?;
        Ok(Expr::Var(token.text().to_owned())) 
    }

    /// literal : INTEGER
    pub fn literal(&mut self) -> Result<Expr, LexerError> {
        let token = self.eat(TokenType::Int)?;
        match token.type_ {
            TokenType::Int => {
                Ok(Expr::Val(token.value()))
            }
            _ => Err(LexerError {
                cause: format!("Expected a literal"),
            }),
        }
    }

    /// factor : prefix
    ///        | literal
    ///        | LPAREN expr RPAREN
    ///        | variable
    pub fn factor(&mut self) -> Result<Expr, LexerError> {
        match self.current_token.type_ {
            TokenType::Op => self.prefix(),
            TokenType::LParen => {
                self.eat(TokenType::LParen)?;
                let expr = self.expr();
                self.eat(TokenType::RParen)?;
                expr
            }
            TokenType::Ident => self.variable(),
            _ => self.literal(),
        }
    }

    /// term : factor ((MUL | DIV) factor)*
    pub fn term(&mut self) -> Result<Expr, LexerError> {
        let mut result = self.factor()?;
        loop {
            match self.current_token.value() {
                Value::Op(op) => {
                    match op {
                        Op::Mult | Op::Div => {
                            self.eat(TokenType::Op).unwrap();
                            result = Expr::BinOp(Box::new(result), op, Box::new(self.factor()?));
                        }
                        _ => break,
                    }
                    continue;
                }
                _ => break,
            }
        }
        Ok(result)
    }

    /// infix : term ((PLUS | MINUS) term)*
    pub fn infix(&mut self) -> Result<Expr, LexerError> {
        let mut result = self.term()?;
        loop {
            match self.current_token.value() {
                Value::Op(op) => {
                    match op {
                        Op::Plus | Op::Minus => {
                            self.eat(TokenType::Op).unwrap();
                            result = Expr::BinOp(Box::new(result), op, Box::new(self.term()?));
                        }
                        _ => break,
                    }
                    continue;
                }
                _ => break,
            }
        }
        Ok(result)
    }

    /// binding_body : IN expr
    ///              | binding
    pub fn binding_body(&mut self) -> Result<Expr, LexerError> {
        match self.current_token.type_ {
            TokenType::In => {
                self.eat(TokenType::In)?;
                self.expr()
            }
            _ => self.binding(),
        }
    }

    /// type_annotation : COLON expr
    pub fn type_annotation(&mut self) -> Result<Expr, LexerError> {
        self.eat(TokenType::Colon)?;
        self.expr()
    }

    /// cannonical_binding : LET variable type_annotation? ASSIGN expr binding_body
    pub fn cannonical_binding(&mut self) -> Result<Expr, LexerError> {
        self.eat(TokenType::Let)?;
        let var = self.eat(TokenType::Ident)?;
        let type_ = if let TokenType::Colon = self.current_token.type_ {
            Some(Box::new(self.type_annotation()?.clone()))
        } else {
            None
        };
        self.eat(TokenType::Assign)?;
        let value = self.expr()?;
        let body = self.binding_body()?;
        Ok(Expr::Binding(
            type_,
            var.text().to_owned(),
            Box::new(value),
            Box::new(body),
        ))
    }

    /// surgared_binding : variable LET_ASSIGN expr binding_body
    pub fn sugared_binding(&mut self) -> Result<Expr, LexerError> {
        let var = self.eat(TokenType::Ident)?;
        self.eat(TokenType::LetAssign)?;
        let type_ = None;
        let value = self.expr()?;
        let body = self.binding_body()?;
        Ok(Expr::Binding(
            type_,
            var.text().to_owned(),
            Box::new(value),
            Box::new(body),
        ))

    }

    /// binding : cannonical_binding
    ///         | sugared_binding
    pub fn binding(&mut self) -> Result<Expr, LexerError> {
        match self.current_token.type_ {
            TokenType::Let => self.cannonical_binding(), 
            _ => self.sugared_binding(), 
        }
    }

    /// expr : ( binding | infix ) ( COLON expr )?
    pub fn expr(&mut self) -> Result<Expr, LexerError> {
        match self.current_token.type_ {
            TokenType::Let => self.cannonical_binding(),
            TokenType::Ident => {
                match self.peek()? {
                    TokenType::LetAssign => self.sugared_binding(),
                    _ => self.infix(),
                }
            }
            _ => self.infix(),
        }
    }

    /// program : expr
    /// expr : ( binding | infix ) type_annotation?
    /// type_annotation : COLON expr
    /// binding : cannonical_binding
    ///         | sugared_binding
    /// cannonical_binding : LET variable type_annotation? ASSIGN expr binding_body
    /// surgared_binding : variable LET_ASSIGN expr binding_body
    /// binding_body : IN expr
    ///              | binding
    /// infix : term ((PLUS | MINUS) term)*
    /// term : factor ((MUL | DIV) factor)*
    /// factor : prefix
    ///        | literal
    ///        | LPAREN expr RPAREN
    ///        | variable
    /// prefix : PLUS factor
    ///        | MINUS factor
    /// literal : INTEGER
    /// variable: ID
    /// literal : INTEGER
    pub fn parse(&mut self) -> Result<AST, LexerError> {
        Ok(AST::Expr(self.expr()?))
    }
}

#[cfg(test)]
mod tests{
    use super::*;

    // term
    #[test]
    fn test_term() {
        let mut parser = Parser::new("-42").unwrap();
        assert_eq!(parser.term().unwrap(), Expr::UnaryOp(Op::Minus, Box::new(Expr::Val(Value::Int(42)))));
        let mut parser = Parser::new("42").unwrap();
        assert_eq!(parser.term().unwrap(), Expr::Val(Value::Int(42)));
        let mut parser = Parser::new("*42").unwrap();
        assert!(parser.term().is_err());
    }

    // factor
    #[test]
    fn test_factor() {
        let mut parser = Parser::new("-42").unwrap();
        assert_eq!(parser.factor().unwrap(), Expr::UnaryOp(Op::Minus, Box::new(Expr::Val(Value::Int(42)))));
        let mut parser = Parser::new("42").unwrap();
        assert_eq!(parser.factor().unwrap(), Expr::Val(Value::Int(42)));
        let mut parser = Parser::new("*42").unwrap();
        assert!(parser.factor().is_err());
    }


    // prefix
    #[test]
    fn test_prefix() {
        let mut parser = Parser::new("-42").unwrap();
        assert_eq!(parser.prefix().unwrap(), Expr::UnaryOp(Op::Minus, Box::new(Expr::Val(Value::Int(42)))));
        let mut parser = Parser::new("+42").unwrap();
        assert_eq!(parser.prefix().unwrap(), Expr::UnaryOp(Op::Plus, Box::new(Expr::Val(Value::Int(42)))));
        let mut parser = Parser::new("*42").unwrap();
        assert!(parser.prefix().is_err());
    }

    // variable
    #[test]
    fn test_variable() {
        let mut parser = Parser::new("hello").unwrap();
        assert_eq!(parser.variable().unwrap(), Expr::Var("hello".to_owned()));
        let mut parser = Parser::new("4hello").unwrap();
        assert!(parser.prefix().is_err());
        let mut parser = Parser::new("let").unwrap();
        assert!(parser.prefix().is_err());
    }

    // literal
    #[test]
    fn test_literal() {
        let mut parser = Parser::new("42").unwrap();
        assert_eq!(parser.literal().unwrap(), Expr::Val(Value::Int(42)));
    }

    // INTEGER
    #[test]
    fn test_integer() {
        let mut parser = Parser::new("42").unwrap();
        assert_eq!(parser.expr().unwrap(), Expr::Val(Value::Int(42)));
    }
}
