use crate::ast::Value;
use std::ops::Range;

#[derive(Debug, Clone, PartialEq, PartialOrd)]
pub enum TokenType {
    Int,
    Op,
    RParen,
    LParen,
    Let,       // let
    In,        // in
    Ident,
    Assign,    // =
    LetAssign, // :=
    Colon,     // :
    EOF,
}

#[derive(Debug, Clone)]
pub struct Token<'t> {
    pub type_: TokenType,
    pub range: Range<usize>,
    pub src: &'t str,
}

impl<'t> Token<'t> {
    // TODO: return a result
    pub fn value(&self) -> Value {
        match &self.type_ {
            TokenType::Int => Value::Int(self.text().parse().unwrap()),
            TokenType::Op => Value::Op(self.text().parse().unwrap()),
            _ => Value::None,
        }
    }

    pub fn text(&self) -> &'t str {
        &self.src[self.range.clone()]
    }
}

pub(crate) struct Lexer<'l> {
    src: &'l str,
}

impl<'l> Lexer<'l> {
    pub fn new(src: &'l str) -> Self {
        Self { src }
    }

    pub fn tokens(&self) -> LexerTokens<'l> {
        LexerTokens::new(self.src)
    }
}

#[derive(Debug)]
pub struct LexerError {
    pub cause: String,
}

pub struct LexerTokens<'lt> {
    src: &'lt str,
    len: usize,
    byte_idx: usize,
    current_char: Option<Range<usize>>,
    current_content: &'lt str,
}

impl<'lt> LexerTokens<'lt> {
    fn new(src: &'lt str) -> Self {
        let mut lt = Self {
            src,
            byte_idx: 0,
            len: src.len(),
            current_char: None,
            current_content: &src[0..0],
        };
        lt.get_current_char();
        lt
    }

    fn peek(&self) -> Option<&'lt str> {
        if self.byte_idx > self.len {
            return None;
        };
        let mut end = self.byte_idx;
        while end < self.len {
            end += 1;
            if self.src.is_char_boundary(end) {
                return Some(&self.src[self.byte_idx..end]);
            }
        }
        return None;
    }

    fn get_current_char(&mut self) {
        let start = self.byte_idx;
        if start > self.len {
            self.current_char = None;
            return;
        };
        while self.byte_idx < self.len {
            self.byte_idx += 1;
            if self.src.is_char_boundary(self.byte_idx) {
                self.current_char = Some(start..self.byte_idx);
                self.current_content = &self.src[start..self.byte_idx];
                return;
            }
        }
        self.byte_idx += 1;
        self.current_char = None;
    }

    fn skip_whitespaces(&mut self) {
        loop {
            match &self.current_char {
                Some(_) => {
                    if self.is_whitespace() {
                        self.get_current_char();
                    } else {
                        break;
                    }
                }
                None => {
                    break;
                }
            }
        }
    }

    fn new_token(&self, type_: TokenType, range: Range<usize>) -> Token<'lt> {
        Token {
            type_,
            range,
            src: self.src,
        }
    }

    fn is_whitespace(&self) -> bool {
        self.current_content
            .bytes()
            .all(|c| c.is_ascii_whitespace())
    }

    fn is_digit(&self) -> bool {
        self.current_content.bytes().all(|c| c.is_ascii_digit())
    }

    fn is_ident_head(&self) -> bool {
        self.current_content.bytes().all(|c| (c == '_' as u8 ) | c.is_ascii_alphabetic())
    }

    fn is_ident_body(&self) -> bool {
        self.current_content.bytes().all(|c| (c == '_' as u8 ) | c.is_ascii_alphanumeric())
    }

    fn ident(&mut self) -> Token<'lt> {
        let c = self.current_char.clone().unwrap();
        let start = c.start;
        let mut end = c.end;
        self.get_current_char();
        while self.is_ident_body() {
            match &self.current_char {
                None => break,
                Some(r) => end = r.end,
            };
            self.get_current_char();
        }
        match &self.src[start..end] {
            "let" => self.new_token(TokenType::Let, start..end),
            "in" => self.new_token(TokenType::In, start..end),
            _ => self.new_token(TokenType::Ident, start..end)
        }
    }

    fn integer(&mut self) -> Token<'lt> {
        let c = self.current_char.clone().unwrap();
        let start = c.start;
        let mut end = c.end;
        self.get_current_char();
        while self.is_digit() {
            match &self.current_char {
                None => break,
                Some(r) => end = r.end,
            };
            self.get_current_char();
        }
        self.new_token(TokenType::Int, start..end)
    }
}

impl<'lt> Iterator for LexerTokens<'lt> {
    type Item = Result<Token<'lt>, LexerError>;

    fn next(&mut self) -> Option<Self::Item> {
        match &self.current_char {
            None => {
                return Some(Ok(
                    self.new_token(TokenType::EOF, self.src.len()..self.src.len())
                ))
            }
            Some(r) => {
                let mut start = r.start;
                while self.current_char.is_some() {
                    if let Some(r) = &self.current_char {
                        if self.is_whitespace() {
                            self.skip_whitespaces();
                            match &self.current_char {
                                None => break,
                                Some(r) => start = r.start,
                            };
                            continue;
                        } else if self.is_ident_head() {
                            return Some(Ok(self.ident()));
                        } else if self.is_digit() {
                            return Some(Ok(self.integer()));
                        } else if self.current_content == "=" {
                            let end = r.end;
                            self.get_current_char();
                            return Some(Ok(self.new_token(TokenType::Assign, start..end)));
                        } else if self.current_content == ":" {
                            let end = r.end;
                            let peek = self.peek();
                            match peek {
                                Some(s) if s == "=" => {
                                    self.get_current_char();
                                    self.get_current_char();
                                    return Some(Ok(self.new_token(TokenType::LetAssign, start..end + 1))) 
                                }
                                _ => {
                                    self.get_current_char();
                                    return Some(Ok(self.new_token(TokenType::Colon, start..end)))
                                }
                            }
                        } else if self.current_content == "(" {
                            let end = r.end;
                            self.get_current_char();
                            return Some(Ok(self.new_token(TokenType::LParen, start..end)));
                        } else if self.current_content == ")" {
                            let end = r.end;
                            self.get_current_char();
                            return Some(Ok(self.new_token(TokenType::RParen, start..end)));
                        } else if "+-*/".contains(self.current_content) {
                            let end = r.end;
                            self.get_current_char();
                            return Some(Ok(self.new_token(TokenType::Op, start..end)));
                        } else {
                            return Some(Err(LexerError {
                                cause: format!("Unknown token: `{}`", self.current_content),
                            }));
                        }
                    }
                }
                self.current_char = None;
                return Some(Ok(self.new_token(TokenType::EOF, start..start)));
            }
        }
    }
}

#[cfg(test)]
mod tests{
    use super::*;

    // Int,
    #[test]
    fn test_int() {
        let src = "42";
        assert_eq!(
            Lexer::new(src)
            .tokens()
            .map(|t| {
                let t = t.unwrap();
                (t.type_.clone(), t.text())
            })
            .take_while(|tt| tt.0 != TokenType::EOF)
            .take(200)
            .collect::<Vec<(TokenType, &str)>>(), [(TokenType::Int, "42")]);
    }

    macro_rules! assert_token_types {
        ($src:literal, $expected:expr) => {
            let src=$src;
            assert_eq!(
                Lexer::new(src)
                .tokens()
                .map(|t| {
                    let t = t.unwrap();
                    (t.type_.clone(), t.text())
                })
                .take_while(|tt| tt.0 != TokenType::EOF)
                .take(200)   // the iterator yields EOF till the end of time.
                .collect::<Vec<(TokenType, &str)>>(), $expected);
        }
    }

    // Op,
    #[test]
    fn test_op() {
        assert_token_types!("+", [(TokenType::Op, "+")]);
        assert_token_types!("-", [(TokenType::Op, "-")]);
        assert_token_types!("*", [(TokenType::Op, "*")]);
        assert_token_types!("/", [(TokenType::Op, "/")]);
        assert_token_types!(" + ", [(TokenType::Op, "+")]);
    }

    // Ident
    #[test]
    fn test_ident() {
        assert_token_types!("hello", [(TokenType::Ident, "hello")]);
        assert_token_types!("Hello", [(TokenType::Ident, "Hello")]);
        assert_token_types!("_hello", [(TokenType::Ident, "_hello")]);
        assert_token_types!("hello_world_", [(TokenType::Ident, "hello_world_")]);
        assert_token_types!(" lettuce ", [(TokenType::Ident, "lettuce")]);
    }

    // Let
    #[test]
    fn test_let() {
        assert_token_types!("let", [(TokenType::Let, "let")]);
        assert_token_types!(" let ", [(TokenType::Let, "let")]);
    }

    // In
    #[test]
    fn test_in() {
        assert_token_types!("in", [(TokenType::In, "in")]);
        assert_token_types!(" in ", [(TokenType::In, "in")]);
    }

    // Assign
    #[test]
    fn test_assign() {
        assert_token_types!("=", [(TokenType::Assign, "=")]);
        assert_token_types!(" = ", [(TokenType::Assign, "=")]);
    }

    // LetAssign
    #[test]
    fn test_let_assign() {
        assert_token_types!(":=", [(TokenType::LetAssign, ":=")]);
        assert_token_types!(" := ", [(TokenType::LetAssign, ":=")]);
    }

    // Colon
    #[test]
    fn test_colon() {
        assert_token_types!(":", [(TokenType::Colon, ":")]);
        assert_token_types!(" : ", [(TokenType::Colon, ":")]);
    }

    // LParen,
    #[test]
    fn test_lparen() {
        assert_token_types!("(", [(TokenType::LParen, "(")]);
        assert_token_types!(" ( ", [(TokenType::LParen, "(")]);
    }

    // RParen,
    #[test]
    fn test_rparen() {
        assert_token_types!(")", [(TokenType::RParen, ")")]);
        assert_token_types!(" ) ", [(TokenType::RParen, ")")]);
    }
}
