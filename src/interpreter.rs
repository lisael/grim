use crate::ast::{Expr, Op, Value, AST};
use crate::lexer::LexerError;
use crate::parser::Parser;
use std::collections::HashMap;

pub struct Interpreter<'i> {
    parser: Parser<'i>,
    global_scope: HashMap<String, Value>,
}

impl<'i> Interpreter<'i> {
    pub fn new(src: &'i str) -> Result<Self, LexerError> {
        Ok(Self {
            parser: Parser::new(src)?,
            global_scope: HashMap::new(),
        })
    }

    fn visit_val(&mut self, e: &Expr) -> Result<Value, LexerError> {
        if let Expr::Val(val) = e {
            Ok(val.clone())
        } else {
            unreachable!()
        }
    }

    fn visit_unary(&mut self, e: &Expr) -> Result<Value, LexerError> {
        if let Expr::UnaryOp(op, val) = e {
            let val = self.visit_expr(&val)?;
            match op {
                Op::Minus => Ok(Value::Int(0 - val.as_int())),
                Op::Plus => Ok(val.clone()),
                _ => unreachable!(),
            }
        } else {
            unreachable!()
        }
    }

    fn visit_binop(&mut self, e: &Expr) -> Result<Value, LexerError> {
        if let Expr::BinOp(lhs, op, rhs) = e {
            let lhs = self.visit_expr(&lhs)?;
            let rhs = self.visit_expr(&rhs)?;
            match op {
                Op::Plus => Ok(Value::Int(lhs.as_int() + rhs.as_int())),
                Op::Minus => Ok(Value::Int(lhs.as_int() - rhs.as_int())),
                Op::Mult => Ok(Value::Int(lhs.as_int() * rhs.as_int())),
                Op::Div => Ok(Value::Int(lhs.as_int() / rhs.as_int())),
            }
        } else {
            unreachable!()
        }
    }

    fn visit_binding(&mut self, e: &Expr) -> Result<Value, LexerError> {
        if let Expr::Binding(_type_, name, value, body) = e {
            let val = self.visit_expr(&value)?;
            let old_val = self.global_scope.insert(name.to_owned(), val);
            let body = self.visit_expr(&body);
            match body {
                Ok(_) => match old_val {
                    Some(val) => {
                        self.global_scope.insert(name.to_owned(), val);
                    }
                    None => {
                        self.global_scope.remove_entry(name);
                    }
                },
                Err(_) => (),
            }
            body
        } else {
            unreachable!()
        }
    }

    fn visit_var(&mut self, e: &Expr) -> Result<Value, LexerError> {
        if let Expr::Var(name) = e {
            if let Some(val) = self.global_scope.get(name) {
                Ok(val.clone())
            } else {
                Err(LexerError {
                    cause: format!("`{}` is not defined", name),
                })
            }
        } else {
            unreachable!()
        }
    }

    fn visit_expr(&mut self, e: &Expr) -> Result<Value, LexerError> {
        dbg!(e);
        match e {
            Expr::UnaryOp(_, _) => self.visit_unary(e),
            Expr::BinOp(..) => self.visit_binop(e),
            Expr::Val(_) => self.visit_val(e),
            Expr::Binding(..) => self.visit_binding(e),
            Expr::Var(_) => self.visit_var(e),
        }
    }

    pub fn interpret(&mut self) -> Result<Value, LexerError> {
        let tree = self.parser.parse()?;
        if let AST::Expr(expr) = tree {
            return self.visit_expr(&expr);
        };
        unreachable!();
    }
}

#[cfg(test)]
mod tests{
    use super::*;

    // test everything we can do, yet. This will probably
    // make an artificialy high coverage number, as all the happy path
    // is used, but without fine asserts.
    #[test]
    fn test_full_program() {
        let mut interpreter = Interpreter::new("let a: int = 3 in let c : (let d = 42 in int) = -12 in b := 2 in (a + b) * a / b - a").unwrap();
        assert_eq!(interpreter.interpret().unwrap(), Value::Int(4));
    }
}
