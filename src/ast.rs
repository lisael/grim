use crate::lexer::LexerError;
use std::str::FromStr;

#[derive(Debug, Clone, PartialEq)]
pub enum Value {
    Int(i128),
    Op(Op),
    None,
}

impl Value {
    pub fn as_int(&self) -> i128 {
        match self {
            Self::Int(i) => *i,
            _ => panic!("Not an int value"),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Op {
    Plus,
    Minus,
    Mult,
    Div,
}

impl FromStr for Op {
    type Err = LexerError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            _ if s == "+" => Ok(Op::Plus),
            _ if s == "-" => Ok(Op::Minus),
            _ if s == "*" => Ok(Op::Mult),
            _ if s == "/" => Ok(Op::Div),
            _ => Err(LexerError {
                cause: format!("'{}' is not an operator", s),
            }),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Expr {
    BinOp(Box<Expr>, Op, Box<Expr>),
    UnaryOp(Op, Box<Expr>),
    Val(Value),
    Binding(Option<Box<Expr>>, String, Box<Expr>, Box<Expr>),
    Var(String),
}

#[derive(Debug, Clone)]
pub enum AST {
    Expr(Expr),
}

#[cfg(test)]
mod tests{
    use super::*;
    use std::str::FromStr;

    #[test]
    fn test_op_fromstr() {
        assert_eq!(Op::from_str("+").unwrap(), Op::Plus);
        assert!(Op::from_str("not_an_op").is_err());
    }
}

