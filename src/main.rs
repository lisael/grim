mod ast;
mod interpreter;
mod lexer;
mod parser;

fn main() {
    let src = std::env::args().nth(1).expect("no pattern given");
    let mut interp = interpreter::Interpreter::new(&src);
    match interp {
        Ok(ref mut interp) => println!("{:?}", interp.interpret()),
        Err(e) => println!("{:?}", e),
    }
}
