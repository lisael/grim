## Example

cannonical representation:

```hs
{* a comment
    {* a nested comment *}
*}

let a: int = 1

let b: int = 2

assert a + b == 3

let AType: RecordType = {
    f1: int = 42
} 

let a: {f1: int, f2: bool} = a / {f2 = false}

assert type(a) == AType / { f2: bool }

let MyRecord: RecordType = {
    field1: int = 42,
    name: str = "",
    field3: bool,
    named: `not .name.isempty`
}

let MyResult: RecordType = {
    titi: int = 12,
}

func DoStuf<R>(r: MyRecord/rho:R) -> MyResult/R {
    (default(MyResult) / {titi = r.field1}) / rho
}

```

With type inference and syntaxic sugar:

```
a := 1

b := 2

assert a + b == 3

let AType = {
    f1: int = 42,
}

let a = AType{f1 = 12}

let a = a / {bb = 13}

assert a^ == AType/{bb: int}

let MyRecord = {
    field1: int = 42,
    name: str = "",
    field3: bool,
    named: `not .name.isempty`
}

let MyResult = {
    titi: int = 12
}

func DoStuf<R>(r: MyRecord/rho:R) -> MyResult/R {
    (MyResult! / {titi = r.field1}) / rho
}
```
